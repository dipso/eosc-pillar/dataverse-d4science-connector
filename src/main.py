#from fastapi import FastAPI
from flask import Flask, redirect, request
import requests
import d4scienceManager
import os
from dotenv import load_dotenv

load_dotenv()


#app = FastAPI()
app = Flask(__name__)
app.debug = True

# dataverseDownloadFileUrl="https://data-preproduction.inrae.fr/api/access/datafile/:persistentId/?persistentId=doi:"
# dataverseBaseUrl="https://data-preproduction.inrae.fr/api"
dataverseDownloadFileUrl = os.environ.get("dataverseDownloadFileUrl")
dataverseBaseUrl = os.environ.get("dataverseBaseUrl")
d4scienceWebsiteUrl = os.environ.get("d4scienceWebsiteUrl")
folderId = os.environ.get("folderId")
dataFromDataInraeVREId = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
slash_url_escape_value = "-"
slash = "/"
two_point = ":"


@app.route("/d4science", methods=['GET'])
def send_data_to_d4science():
    # extract filePid from request
    if 'filePid' in request.args:
        filePid = request.args['filePid']
        print("filePid is:"+filePid)
    else:
        return "The file identifier is required"

    # extrcat datasetPid
    if 'datasetPid' in request.args:
        datasetPid = request.args['datasetPid']
        print("datasetPid is:"+datasetPid)
    else:
        return "The dataset identifier is required"

    # import data from dataverse to d4science
    url = import_data_from_dataverse_to_d4science(filePid, datasetPid)
    print(url)
    # redirect to D4science
    # return redirect(d4scienceWebsiteUrl)
    return redirect(url, code="302")

# import the file of the dataset from dataverse to d4science VRE


def import_data_from_dataverse_to_d4science(filePid, datasetPid):

    # Extract PId
    if two_point in filePid:
        filePid_tab = filePid.split(two_point)
        if len(filePid_tab) >= 2:
            filePid = filePid_tab[1]

    if two_point in datasetPid:
        datasetPid_tab = datasetPid.split(two_point)
        if len(datasetPid_tab) >= 2:
            datasetPid = datasetPid_tab[1]

    filePidWithOutSlash = filePid
    datasetPidWithoutSlash = datasetPid

    if slash in filePid:
        filePidWithOutSlash = filePid.replace(slash, slash_url_escape_value)

    if slash in datasetPid:
        datasetPidWithoutSlash = datasetPid.replace(
            slash, slash_url_escape_value)

    # Ajout de vérification des paramètres : TO DO Michel

    # Verifier si un dossier ayant le nom du dataset est dejà sur D4science
    datasetIdInVre = d4scienceManager.isDirectoryOrFileExist(
        datasetPidWithoutSlash, folderId)
    # print(datasetIdInVre)
    # Créer eventuellement le dossier
    if (datasetIdInVre == "0"):
        datasetIdInVre = d4scienceManager.createFolder(
            datasetPidWithoutSlash, "test upload de fichier", False, folderId)

    # récuperer les metadata du fichier sur dataverse
    print("filePid is :"+filePid)
    fileMetadata = getFileMetadata(filePid)

    # download du fichier
    download_dataverse_file(filePid, fileMetadata.fileName)

    # Uploader le fichier
    d4scienceManager.uploadFile(
        fileMetadata.fileName, fileMetadata.description, datasetIdInVre)

    # rediriger vers la page web d4science
    #OpenSameTab = '<script language="JavaScript" type="text/JavaScript">window.location = \'%s\';</script>'
    #OpenSameTab % 'https://eosc-pillar.d4science.org/group/eosc-pillar-gateway/workspace'
    # target url : https://eosc-pillar.d4science.org/group/eosc-pillar-gateway/workspace?itemid=abc6e4fe-0e5a-4485-95cf-3aafe0205dd9&operation=gotofolder
    # controller=webbrowser.get(using=None)
    # OpenSameTab % 'd4scienceWebsiteUrl'return controller.open(d4scienceWebsiteUrl,0)
    # return webbrowser.open(d4scienceWebsiteUrl, new=2)
    return d4scienceWebsiteUrl + "?itemid=" + datasetIdInVre + "&operation=gotofolder"


# Download a file from
def download_dataverse_file(filePid, fileName):
    finalDataverseUrl = dataverseDownloadFileUrl+filePid
    response = requests.get(finalDataverseUrl, allow_redirects=True)
    open(fileName, 'wb').write(response.content)


# Get the metadata of file knowing its persistent identifier
def getFileMetadata(filePid):
    completeResourceUrl = dataverseBaseUrl + \
        "/files/:persistentId/metadata?persistentId=doi:"+filePid
    response = requests.get(completeResourceUrl)
    data = response.json()
    fileMetadata = DataverseFileMetadata()
    fileMetadata.fileName = data['label']

    if hasattr(data, 'description'):
        fileMetadata.description = data['description']
    # print("FileName "+fileMetadata.fileName +
    #       " and description : "+fileMetadata.description)
    return fileMetadata


class DataverseFileMetadata:
    fileName = ""
    description = ""

    def __init(self, fileLabel, fileDescription):
        self.fileName = fileLabel
        self.description = fileDescription


 # on running python app.py
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
