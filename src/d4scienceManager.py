#!/usr/bin/env python3

import os
import requests
from dotenv import load_dotenv
import sys

load_dotenv()

d4scienceToken = os.environ.get("d4scienceToken")
d4scienceBaseUrl = os.environ.get("d4scienceBaseUrl")
dataFromDataInraeId = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
folderId = os.environ.get("folderId")
slash_url_escape_value = "-"
slash = "/"

# List the content of directory


def listDirectory():
    completeResourceUrl = d4scienceBaseUrl + '?gcube-token=' + d4scienceToken
    # print("complete request:" + completeResourceUrl)
    response = requests.get(completeResourceUrl)


# find a file or directory knowing it parent id


def isDirectoryOrFileExist(name, parentFolderId):
    itemD4ScienceId = 0
    if slash in name:
        name = name.replace(slash, slash_url_escape_value)
    resourceURI = '/items/'+parentFolderId + \
        '/items/'+name+'?gcube-token='+d4scienceToken
    completeResourceUrl = d4scienceBaseUrl+resourceURI
    print("complete request send to d4science :"+completeResourceUrl)
    response = requests.get(completeResourceUrl)

    data = []
    try:
        data = response.json()

    except:
        print("not found", response)

    if (data == []):
        dataSize = 0
    else:
        dataSize = len(data['itemlist'])

    print("ItemList size is " + str(dataSize))
    if(dataSize == 1):
        itemD4ScienceId = data['itemlist'][0]['id']
    else:
        itemD4ScienceId = '0'
        print(itemD4ScienceId)
    return itemD4ScienceId

# upload a file to the VRE folder that id is known


def uploadFile(fileName, description, parentFolderId):
    if slash in fileName:
        fileName = fileName.replace(slash, slash_url_escape_value)
    resourceURI = '/items/'+parentFolderId + \
        '/create/FILE?gcube-token='+d4scienceToken
    absoluteResourceURI = d4scienceBaseUrl+resourceURI
    print("The post request send is "+absoluteResourceURI)
    fileToUpload = open(fileName, 'rb')
    fileDict = {'name': fileName,
                'description': description, 'file': fileToUpload}
    getdata = requests.post(absoluteResourceURI, files=fileDict)
    statusCode = getdata.status_code
    print("response is :" + str(statusCode))

# Create a new folder in the VRE workspace knowing its parentFolderId and the folder metadata


def createFolder(name, description, hidden, parentFolderId):
    if slash in name:
        name = name.replace(slash, slash_url_escape_value)
    createFolderUri = '/items/'+parentFolderId + \
        '/create/FOLDER?gcube-token='+d4scienceToken
    cfAbsoluteResourceURI = d4scienceBaseUrl+createFolderUri
    # print("The post request for creating a folder is "+cfAbsoluteResourceURI)
    payload = {'name': name, 'description': description, 'hidden': hidden}
    response = requests.post(cfAbsoluteResourceURI, data=payload)
    print(cfAbsoluteResourceURI, name)
    statusCode = response.status_code
    print("response is :" + str(statusCode))
    # response_Json = response.json()
    # print("Response receive from api: "+response.text)
    return response.text

# Delete an item in the VRE workspace knowing its id


def deleteItem(itemId):
    deleteURI = '/items/'+itemId+'?force=true&gcube-token=' + d4scienceToken
    absoluteDeleteURI = d4scienceBaseUrl+deleteURI
    response = requests.delete(absoluteDeleteURI)
    print(response.text)
