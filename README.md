# dataverse-d4science-connector

This Python based connector unables data transfert between a Dataverse server and D4Science (https://www.d4science.org/).

### Dataverse External Tools

Once your connector is online, make sure to update the Dataverse External Tools on your dataverse server by adding at tool pointing to your connector :

https://{{your connector domain name}}/d4science/?filePid={{the filePid from the external tools query}}

### To setup the environnement variables, please follow this template:

```

d4scienceToken='{{HERE_YOUR_D4SCIENCE_TOKEN}}'
dataverseDownloadFileUrl='https://{{ YOUR DATAVERSE SERVER}}/api/access/datafile/:persistentId/?persistentId=doi:'
dataverseBaseUrl="https://{{ YOUR DATAVERSE SERVER}}/api"
d4scienceWebsiteUrl="https://{{ YOUR D4SCIENCE URL WORKSPACE }}"

```

You can then create a new Gitlab environnement file with this template via Settings -> CI/CD -> Variables

Remember to add this line at the end od the Dockerfile for the environnement variables to be included in the image

```
COPY .env .env

```

### Deployment

The deployment of this connector was tested with Gitlab's CI/CD configuration + Kubernetes pods.
The pipeline has 2 stages, build and deploy. In the build stage, we create the docker image of the node application and we push it to gitlab registry.
In the deploy stage, we use the build image to deploy an Helm chart to kubernetes cluster.
The pipeline is executed with gitlab runner installed in orion kubernetes cluster. The url of the deployment appears in the end of the logs of successful execution of the pipeline.

This connector can be deployed in a simpler environnement as long as the env file is accessible.
